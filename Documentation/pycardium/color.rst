``color`` - Colors
==================

Color Class
-----------

.. automodule:: color
   :members:

Constants
---------

The color module also contains a few constanst for commonly used colors:

.. py:data:: color.BLACK
.. py:data:: color.WHITE
.. py:data:: color.RED
.. py:data:: color.GREEN
.. py:data:: color.YELLOW
.. py:data:: color.BLUE
.. py:data:: color.MAGENTA
.. py:data:: color.CYAN

``htmlcolor`` - Color Constants
===============================
The ``htmlcolor`` module contains even more color constants.  Note
that loading the ``htmlcolor`` module will require ~12K of RAM.

.. py:data:: htmlcolor.ALICEBLUE
.. py:data:: htmlcolor.ANTIQUEWHITE
.. py:data:: htmlcolor.AQUA
.. py:data:: htmlcolor.AQUAMARINE
.. py:data:: htmlcolor.AZURE
.. py:data:: htmlcolor.BEIGE
.. py:data:: htmlcolor.BISQUE
.. py:data:: htmlcolor.BLACK
.. py:data:: htmlcolor.BLANCHEDALMOND
.. py:data:: htmlcolor.BLUE
.. py:data:: htmlcolor.BLUEVIOLET
.. py:data:: htmlcolor.BROWN
.. py:data:: htmlcolor.BURLYWOOD
.. py:data:: htmlcolor.CADETBLUE
.. py:data:: htmlcolor.CHARTREUSE
.. py:data:: htmlcolor.CHOCOLATE
.. py:data:: htmlcolor.CORAL
.. py:data:: htmlcolor.CORNFLOWERBLUE
.. py:data:: htmlcolor.CORNSILK
.. py:data:: htmlcolor.CRIMSON
.. py:data:: htmlcolor.CYAN
.. py:data:: htmlcolor.DARKBLUE
.. py:data:: htmlcolor.DARKCYAN
.. py:data:: htmlcolor.DARKGOLDENROD
.. py:data:: htmlcolor.DARKGRAY
.. py:data:: htmlcolor.DARKGREEN
.. py:data:: htmlcolor.DARKKHAKI
.. py:data:: htmlcolor.DARKMAGENTA
.. py:data:: htmlcolor.DARKOLIVEGREEN
.. py:data:: htmlcolor.DARKORANGE
.. py:data:: htmlcolor.DARKORCHID
.. py:data:: htmlcolor.DARKRED
.. py:data:: htmlcolor.DARKSALMON
.. py:data:: htmlcolor.DARKSEAGREEN
.. py:data:: htmlcolor.DARKSLATEBLUE
.. py:data:: htmlcolor.DARKSLATEGRAY
.. py:data:: htmlcolor.DARKTURQUOISE
.. py:data:: htmlcolor.DARKVIOLET
.. py:data:: htmlcolor.DEEPPINK
.. py:data:: htmlcolor.DEEPSKYBLUE
.. py:data:: htmlcolor.DIMGRAY
.. py:data:: htmlcolor.DODGERBLUE
.. py:data:: htmlcolor.FIREBRICK
.. py:data:: htmlcolor.FLORALWHITE
.. py:data:: htmlcolor.FORESTGREEN
.. py:data:: htmlcolor.FUCHSIA
.. py:data:: htmlcolor.GAINSBORO
.. py:data:: htmlcolor.GHOSTWHITE
.. py:data:: htmlcolor.GOLD
.. py:data:: htmlcolor.GOLDENROD
.. py:data:: htmlcolor.GRAY
.. py:data:: htmlcolor.GREEN
.. py:data:: htmlcolor.GREENYELLOW
.. py:data:: htmlcolor.HONEYDEW
.. py:data:: htmlcolor.HOTPINK
.. py:data:: htmlcolor.INDIANRED
.. py:data:: htmlcolor.INDIGO
.. py:data:: htmlcolor.IVORY
.. py:data:: htmlcolor.KHAKI
.. py:data:: htmlcolor.LAVENDER
.. py:data:: htmlcolor.LAVENDERBLUSH
.. py:data:: htmlcolor.LAWNGREEN
.. py:data:: htmlcolor.LEMONCHIFFON
.. py:data:: htmlcolor.LIGHTBLUE
.. py:data:: htmlcolor.LIGHTCORAL
.. py:data:: htmlcolor.LIGHTCYAN
.. py:data:: htmlcolor.LIGHTGOLDENRODYELLOW
.. py:data:: htmlcolor.LIGHTGRAY
.. py:data:: htmlcolor.LIGHTGREEN
.. py:data:: htmlcolor.LIGHTPINK
.. py:data:: htmlcolor.LIGHTSALMON
.. py:data:: htmlcolor.LIGHTSEAGREEN
.. py:data:: htmlcolor.LIGHTSKYBLUE
.. py:data:: htmlcolor.LIGHTSLATEGRAY
.. py:data:: htmlcolor.LIGHTSTEELBLUE
.. py:data:: htmlcolor.LIGHTYELLOW
.. py:data:: htmlcolor.LIME
.. py:data:: htmlcolor.LIMEGREEN
.. py:data:: htmlcolor.LINEN
.. py:data:: htmlcolor.MAGENTA
.. py:data:: htmlcolor.MAROON
.. py:data:: htmlcolor.MEDIUMAQUAMARINE
.. py:data:: htmlcolor.MEDIUMBLUE
.. py:data:: htmlcolor.MEDIUMORCHID
.. py:data:: htmlcolor.MEDIUMPURPLE
.. py:data:: htmlcolor.MEDIUMSEAGREEN
.. py:data:: htmlcolor.MEDIUMSLATEBLUE
.. py:data:: htmlcolor.MEDIUMSPRINGGREEN
.. py:data:: htmlcolor.MEDIUMTURQUOISE
.. py:data:: htmlcolor.MEDIUMVIOLETRED
.. py:data:: htmlcolor.MIDNIGHTBLUE
.. py:data:: htmlcolor.MINTCREAM
.. py:data:: htmlcolor.MISTYROSE
.. py:data:: htmlcolor.MOCCASIN
.. py:data:: htmlcolor.NAVAJOWHITE
.. py:data:: htmlcolor.NAVY
.. py:data:: htmlcolor.OLDLACE
.. py:data:: htmlcolor.OLIVE
.. py:data:: htmlcolor.OLIVEDRAB
.. py:data:: htmlcolor.ORANGE
.. py:data:: htmlcolor.ORANGERED
.. py:data:: htmlcolor.ORCHID
.. py:data:: htmlcolor.PALEGOLDENROD
.. py:data:: htmlcolor.PALEGREEN
.. py:data:: htmlcolor.PALETURQUOISE
.. py:data:: htmlcolor.PALEVIOLETRED
.. py:data:: htmlcolor.PAPAYAWHIP
.. py:data:: htmlcolor.PEACHPUFF
.. py:data:: htmlcolor.PERU
.. py:data:: htmlcolor.PINK
.. py:data:: htmlcolor.PLUM
.. py:data:: htmlcolor.POWDERBLUE
.. py:data:: htmlcolor.PURPLE
.. py:data:: htmlcolor.RED
.. py:data:: htmlcolor.ROSYBROWN
.. py:data:: htmlcolor.ROYALBLUE
.. py:data:: htmlcolor.SADDLEBROWN
.. py:data:: htmlcolor.SALMON
.. py:data:: htmlcolor.SANDYBROWN
.. py:data:: htmlcolor.SEAGREEN
.. py:data:: htmlcolor.SEASHELL
.. py:data:: htmlcolor.SIENNA
.. py:data:: htmlcolor.SILVER
.. py:data:: htmlcolor.SKYBLUE
.. py:data:: htmlcolor.SLATEBLUE
.. py:data:: htmlcolor.SLATEGRAY
.. py:data:: htmlcolor.SNOW
.. py:data:: htmlcolor.SPRINGGREEN
.. py:data:: htmlcolor.STEELBLUE
.. py:data:: htmlcolor.TAN
.. py:data:: htmlcolor.TEAL
.. py:data:: htmlcolor.THISTLE
.. py:data:: htmlcolor.TOMATO
.. py:data:: htmlcolor.TURQUOISE
.. py:data:: htmlcolor.VIOLET
.. py:data:: htmlcolor.WHEAT
.. py:data:: htmlcolor.WHITE
.. py:data:: htmlcolor.WHITESMOKE
.. py:data:: htmlcolor.YELLOW
.. py:data:: htmlcolor.YELLOWGREEN
